\chapter{Hypothesis Testing}

\section{Introduction to Hypothesis Testing}

We have seen how to use confidence intervals to make inferences back
on the population, however, the standard method for doing this is
by hypothesis testing. Hypothesis testing is used to make judgements
about the value of a parameter. Unlike confidence intervals, which
give us a range of values to estimate the true parameter, the results
of a hypothesis test only give evidence to support one of two conclusions: reject our previous belief or do not reject our previous belief.

Hypothesis testing begins with two statements: the null hypothesis and alternative hypothesis. The null hypothesis is that nothing is going on, no change has happened, use the previously held belief, etc. The alternative hypothesis is what we believe the true value to be or to show what the true parameter is not. It is the new standard we want to use for the parameter of interest.

Hypothesis testing has its own form of commonly used notation. $H_{0}$
is used to represent the null hypothesis and $H_{a}$ is used to represent the alternative hypothesis. Using an example of one population mean, they are presented in the following form

\begin{align*}
H_{0}&:\mu=\mu_{0} \\
H_{a}&:\mu\neq\mu_{0}
\end{align*}

In words we might say, ``The null hypothesis is that the population
mean for our variable is equal to value ($\mu_{0}$) and the alternative hypothesis is that the population mean for our variable is not equal to value ($\mu_{0}$)''.

An important thing to note is that the null hypothesis always uses
an equal sign and the alternative can use one of $\neq$, $<$, or
$>$. So, the null always uses an ``equals'', but the alternative
never uses an ``equals''. Specifically we need either a ``not equals'',
``strictly greater than'', or ``strictly less than'' for the alternative.

In general, we choose one of the following hypothesis tests

\begin{align*}
H_{0}&:parameter=specific\, value &  & Two\, Tailed\, Test\\
H_{a}&:parameter\neq specific\, value, & or\\
\\
H_{0}&:parameter=specific\, value &  & One\, Tailed\, Test\\
H_{a}&:parameter>specific\, value, & or\\
\\
H_{0}&:parameter=specific\, value &  & One\, Tailed\, Test\\
H_{a}&:parameter<specific\, value
\end{align*}

\subsubsection{Null Hypothesis}

\begin{itemize}
\item Specifies a single value for the parameter
\item Assumes no change, nothing going on
\item Notation: $H_{0}:parameter=specific\, value$
\item Must always have equal sign
\end{itemize}

\subsubsection{Alternative Hypothesis}

\begin{itemize}
\item Reflects purpose of the test
\item Is what we are looking for, what we truly believe
\item Notation

\begin{itemize}
\item $H_{a}:parameter\neq specific\, value$
\item $H_{a}:parameter>specific\, value$
\item $H_{a}:parameter<specific\, value$
\end{itemize}

\item Must never use an equal sign
\end{itemize}

\subsubsection{Hypothesis Testing Conclusions}

Hypothesis tests can only come to the conclusion of rejecting the
null hypothesis or not rejecting the null hypothesis. We never accept
the null or alternative or believe the results to be the ``truth''.
The possible outcomes of our situation are as follows:

\begin{tabular}{|c|c|c|c|}
\cline{3-4} 
\multicolumn{1}{c}{} &  & \multicolumn{2}{c|}{$H_{0}$ is} \\
\cline{3-4} 
\multicolumn{1}{c}{} &  & True & False \\
\hline 
\multirow{2}{*}{Decision} & Do not reject $H_{0}$ & Correct & Type II error \\
\cline{2-4} 
 & Reject $H_{0}$ & Type I error & Correct \\
\hline 
\end{tabular}

A type I error is when we reject the null hypothesis when it is in
fact the truth. A type II error is when we do not reject the null
hypothesis when it is in fact false. We represent the probability
of making a type I error with $\alpha$. Whenever you run a hypothesis
test, you must first decide upon a value for $\alpha$. The standard
is to use $\alpha=0.05.$

\subsubsection{$p$-value}

The $p$-value is the calculated value that makes our decision to
reject or not reject the null hypothesis.

\begin{defn}
$p$-value: The probability of observing our sample data, or data
more extreme, given that the null hypothesis is true.
\end{defn}

If a $p$-value is calculated to be less than 0.05, for $\alpha = 0.05$, then we reject the null hypothesis. If the $p$-value is greater than 0.05 then we do not reject the null hypothesis. In other words, $p\leq0.05$ means there is sufficient evidence to support the alternative hypothesis.

A $p>0.05$ shows insufficient evidence to support the alternative
hypothesis.

\subsubsection{CI vs HT}

\begin{enumerate}
\item [{CI}]~
\noindent \begin{flushleft}
Confidence intervals are used to estimate the population parameter.
It results in a range of possible values. Because of this, we can
easily assess any practical significance from the statistical significance.
\par\end{flushleft}

\item [{HT}]~
\noindent \begin{flushleft}
Hypothesis tests are used to assess evidence provided by data to favor
some claim of the population. The resulting $p$-value gives no information on the magnitude of the practical significance. Large samples are especially prone to presenting impractical significant differences as very small differences will be magnified by the very small standard error.
\par\end{flushleft}
\end{enumerate}

\section{Steps Behind Hypothesis Testing}

Hypothesis testing consists of seven important steps:

\begin{enumerate}[Step 1:]
\item Choose the $\alpha$ level for the test
\item Determine the null and alternative hypothesis
\item Check the hypothesis test assumptions
\item Calculate the relevant sample statistics
\item Calculate the $p$-value
\item Determine if the null hypothesis is rejected or not rejected
\item Report the conclusion in context of the situation
\end{enumerate}

\subsection{Step 1: Choose the $\alpha$ level for the test}

Let $\alpha$ be the type I error we're allowing into our test. $\alpha$ is always chosen before conducting the hypothesis test. The usual choice is to let $\alpha=0.05$.

There are trade-offs for choosing a high or low $\alpha$ level. If the consequences of a type I error are very serious then a very small significance level is appropriate. However, a very small $\alpha$ level has a much higher chance of making a type II error. So, if making a type II error has serious consequences and the consequences of making a type I error are not very serious, then a larger significance level is appropriate.

There are methods to find the best solution for a particular situation using power analysis.  Power is defined as $1-P(\mathrm{Type \ II \ Error})$.  In words, power is the probability of rejecting the null hypothesis when in reality the null hypothesis really is false.

\subsection{Step 2: Determine the null and alternative hypothesis}

As seen before, the null hypothesis is the assumption that the current belief is the truth or that nothing is happening.  The alternative hypothesis is the new belief that the researcher is trying to prove or that there is something happening. Some examples of null and alternative hypothesis are the following:

\begin{align*}
H_0&: \mathrm{There \ is \ no \ difference \ between \ the \ mean \ pulse \ rates \ of \ men \ and \ women} \\
H_a&: \mathrm{There \ is \ a \ difference \ between \ the \ mean \ pulse \ rates \ of \ men \ and \ women} \\
\\
H_0&: \mathrm{There \ is \ no \ difference \ between \ the \ proportion \ of \ side \ effects \ for \ two \ different \ drugs} \\
H_a&: \mathrm{There \ is \ a \ difference \ between \ the \ proportion \ of \ side \ effects \ for \ two \ different \ drugs} \\
\\
H_0&: \mathrm{There \ is \ no \ relationship \ between \ exercise \ intensity \ and \ the \ resulting \ aerobic \ benefit} \\
H_a&: \mathrm{There \ is \ a \ relationship \ between \ exercise \ intensity \ and \ the \ resulting \ aerobic \ benefit} \\
\\
H_0&: \mathrm{The \ average \ GPA \ of \ UW \ students \ is \ 3.0} \\
H_a&: \mathrm{The \ average \ GPA \ of \ UW \ students \ is \ not \ 3.0} \\
\\
H_0&: \mathrm{There \ is \ no \ difference \ between \ the \ body \ temperature \ of \ men \ and \ women} \\
H_a&: \mathrm{The \ body \ temperature \ of \ men \ is \ less \ than \ women}
\end{align*}
\begin{align*}
H_0&: \mu = \mu_0 \\
H_a&: \mu \neq \mu_0 \\
& \\
H_0&: \mu_1-\mu_2 = 0 \\
H_a&: \mu_1-\mu_2 \neq 0 \\
& \\
H_0&: \pi_1-\pi_2 = 0 \\
H_a&: \pi_1-\pi_2 \neq 0 \\
& \\
H_0&: \mu = \mu_0 \\
H_a&: \mu < \mu_0 \\
& \\
H_0&: \pi_1-\pi_2 = 0 \\
H_a&: \pi_1-\pi_2 > 0
\end{align*}

The hypothesis can be summarized using a sentence or with symbols. When summarizing using a sentence, you must include the parameter and variable for the test.  When summarizing using symbols the correct parameter notation must be used.

\subsection{Step 3: Check the hypothesis test assumptions}

As seen before with confidence intervals, certain assumptions, that the procedure is based on, must be checked. If these assumptions are not met then the conclusion of the test may not be valid. Assumptions are specific for each test and can be found in the upcoming sections.

\subsection{Step 4: Calculate the relevant sample statistics}

Just as with confidence intervals, we will be calculating sample means, sample proportions, sample standard deviations, and now sample correlations. Exactly what needs to be calculated depends on the type of hypothesis test being conducted.

\subsection{Step 5: Calculate the $p$-value}

The $p$-value is the probability of observing our sample data, or data more extreme, given that the null hypothesis is true.  Probability comes into play with distributions.  We've seen the $t$-distribution and $z$-distribution, and the next distribution we will be using is called the $\chi^2$-distribution (Chi-square distribution). The probability that we calculate from these distributions will be under the assumption that what we stated in the null hypothesis is the truth.

First, we need to find the corresponding $t$-score, $z$-score, and $\chi^2$-score for the test. these scores will be known as ''test statistics''. Test statistics are usually of the form

\begin{align*}
\mathrm{Test \ Statistic} = \frac{\mathrm{Sample \ Statistic} - \mathrm{Null \ Population \ Value}}{\mathrm{Null \ Standard \ Error}}
\end{align*}

The probability of observing that score, or scores more extreme, can then be calculated.

\subsection{Step 6: Determine if the null hypothesis is rejected or not rejected}

We will reject the null hypothesis if our $p$-value is less than $\alpha$.  We will not reject the null hypothesis if our $p$-value is greater than $\alpha$.

\subsection{Step 7: Report the conclusion in context of the situation}

The conclusion is worded in terms of the alternative hypothesis. If we reject the null hypothesis, then we can conclude that there is significant evidence for the alternative.  If we do not reject the null hypothesis, then we conclude there is insufficient evidence for the alternative hypothesis.

\section{Hypothesis Tests for One Population Mean}

Hypothesis tests for one population mean are conducted when we have taken one sample and have one numerical variable. The conclusion from the one population mean hypothesis test should be exactly the same as the conclusion from a confidence interval.  The only difference is that the confidence interval will allow us to assess the practical significance of any results.

\begin{enumerate}[Step 1:]
\item The null hypothesis is that the population mean is equal to the previously held belief. The alternative hypothesis is what the researcher is trying to show. They might be trying to show that the null value is simply not true, that the null value is too large, or that the null value is too small. This can be summarized using one of the following methods:

\begin{align*}
H_0&: \mu = \mu_0 \hspace{.1in} vs. \hspace{.1in} H_a: \mu \neq \mu_0 \\
&\mathrm{or} \\
H_0&: \mu = \mu_0 \hspace{.1in} vs. \hspace{.1in} H_a: \mu < \mu_0 \\
&\mathrm{or} \\
H_0&: \mu = \mu_0 \hspace{.1in} vs. \hspace{.1in} H_a: \mu > \mu_0 \\
&\mathrm{or \ in \ sentence \ form} \\
H_0&: \mathrm{The \ population \ mean \ for \ our \ variable \ is \ equal \ to \ the \ null \ value.} \ vs. \\
H_a&: \mathrm{The \ population \ mean \ for \ our \ variable \ is \ not \ equal \ to \ the \ null \ value.} \\
&\mathrm{or} \\
H_0&: \mathrm{The \ population \ mean \ for \ our \ variable \ is \ equal \ to \ the \ null \ value.} \ vs. \\
H_a&: \mathrm{The \ population \ mean \ for \ our \ variable \ is \ less \ than \ the \ null \ value.} \\
&\mathrm{or} \\
H_0&: \mathrm{The \ population \ mean \ for \ our \ variable \ is \ equal \ to \ the \ null \ value.} \ vs. \\
H_a&: \mathrm{The \ population \ mean \ for \ our \ variable \ is \ greater \ than \ the \ null \ value.}
\end{align*}

Note that $\mu_0$ is the notation to represent the general form of the population means null value. We will be plugging in actual values for $\mu_0$ in an actual hypothesis test.

\item The assumptions for the hypothesis test are the same as the assumptions for the one population mean confidence interval.

\begin{enumerate}[1)]
\item Data collected with an SRS (or any appropriate sampling technique)
\item Variable is interval/ratio
\item A normal population distribution. Or, for any type of population distribution, a large sample size, $n \geq 30$, is required.
\end{enumerate}

Since we can never know the exact population distribution, how can we determine if it's normal or not normal?  The sample from the population is designed to be representative of the population.  So, if the sample distribution has a normal shape, then it is reasonable to assume the population is also normal.  If the sample distribution is not normal, then it is reasonable to assume the population distribution is also non-normal.

\item We will need to calculate $\bar{x}, s, \mathrm{and} \ n$ from the data that was collected in the sample. \\

\item To calculate the p-value, the first thing we need to calculate is the test statistic.  The sample mean, $\bar{x}$,  will be used to estimate the population mean $\mu$. The standard error of the sample mean, $\frac{s}{\sqrt{n}}$, will be used to estimate the null standard error.

%\begin{note}
%Notice that our estimate for the null standard error is only a function %of our sample data and doesn't have anything to do with our previous %belief $\mu_0$.  This is because we are assuming a normal sampling %distribution, and the mean and standard deviation are independent for %normal distributions!
%\end{note} 

Our test statistic will be called the ``$t$-statistic'' (We are using ``$t$'' because this is a score from the ``$t$''-distribution) and is calculated as follows:

\begin{align*}
\mathrm{t \text{-} statistic} &= \frac{\bar{x} - \mu_0}{\frac{s}{\sqrt{n}}}
\end{align*}

The $p$-value is the probability of observing your $t$-statistic, or values more extreme.

\begin{enumerate}[a)]
\item To calculate $p$-values for a two sided test (the alternative hypothesis used $\neq$ or ``not equal to''), we must find the probability of observing the $t$-statistic or values larger than the $t$-statistic plus the probability of observing values less than the negative value of the $t$-statistic. In notation

\begin{align*}
p &= P(\mathrm{Observing \ t-statistic \ or \ greater}) + P(\mathrm{Observing \ -(t-statistic) \ or \ smaller})
\end{align*}
\end{enumerate}

\item  Blahblahblah

\end{enumerate}

\section{Hypothesis Tests for Two Dependent Population Means}

Hypothesis tests for two dependent population means are conducted when we have taken two samples and have two dependent numerical variables. After we have calculated the differences, it can be considered a one sample, one numerical variable test. The conclusion from the two dependent means hypothesis test should be exactly the same as the conclusion from a confidence interval.  The only difference is that the confidence interval will allow us to assess the practical significance of any results.

\begin{enumerate}[Step 1:]
\item The null hypothesis is that the population mean difference is equal to zero or some previously held belief. This can be summarized using the following two methods:

\begin{align*}
H_0&: \mu_D = \mu_{D, 0} \\
&\mathrm{or \ in \ sentence \ form} \\
H_0&: \mathrm{The \ population \ mean \ difference \ for \ our \ variables \ is \ equal \ to \ the \ previously \ believed \ value.}
\end{align*}

Note that $\mu_{D, 0}$ is the notation to represent the general form of the population mean difference null value. \\
The alternative hypothesis is what the researcher is trying to show. They might be trying to show that the null value is simply not true, that the null value is too large, or that the null value is too small. So, we will end up with one of the following hypothesis:

\begin{align*}
H_a&: \mu_D \neq \mu_{D, 0} \\
&\mathrm{or} \\
H_a&: \mu_D < \mu_{D, 0} \\
&\mathrm{or} \\
H_a&: \mu_D > \mu_{D, 0} \\
&\mathrm{in \ sentence \ form} \\
H_a&: \mathrm{The \ population \ mean \ difference \ for \ our \ variables \ is \ not \ equal \ to \ the \ previously \ believed \ value.} \\
&\mathrm{or} \\
H_a&: \mathrm{The \ population \ mean \ difference \ for \ our \ variables \ is \ less \ than \ the \ previously \ believed \ value.} \\
&\mathrm{or} \\
H_a&: \mathrm{The \ population \ mean \ difference \ for \ our \ variables \ is \ greater \ than \ the \ previously \ believed \ value.}
\end{align*}

\item The assumptions for the hypothesis test are the same as the assumptions for the two dependent means confidence interval.

\end{enumerate}

\section{Hypothesis Tests for Two Independent Population Means}
Hypothesis tests for two independent population means are conducted when we have taken two samples and have two independent numerical variables. The conclusion from the two independent means hypothesis test should be exactly the same as the conclusion from a confidence interval.  The only difference is that the confidence interval will allow us to assess the practical significance of any results.

\begin{enumerate}[Step 1:]
\item The null hypothesis is that the difference of the population means is equal to zero or some previously held belief. This can be summarized using the following two methods:

\begin{align*}
H_0&: \mu_1 - \mu_2 = (\mu_1 - \mu_2)_0 \\
&\mathrm{or \ in \ sentence \ form} \\
H_0&: \mathrm{The \ difference \ of \ population \ means \ for \ our \ variables \ is \ equal \ to \ the \ previously \ believed \ value.}
\end{align*}

Note that $(\mu_1 - \mu_2)_0$ is the notation to represent the general form of the population difference of means null value. \\
The alternative hypothesis is what the researcher is trying to show. They might be trying to show that the null value is simply not true, that the null value is too large, or that the null value is too small. So, we will end up with one of the following hypothesis:

\begin{align*}
H_a&: \mu_1 - \mu_2 \neq (\mu_1 - \mu_2)_0 \\
&\mathrm{or} \\
H_a&: \mu_1 - \mu_2 < (\mu_1 - \mu_2)_0 \\
&\mathrm{or} \\
H_a&: \mu_1 - \mu_2 > (\mu_1 - \mu_2)_0 \\
&\mathrm{in \ sentence \ form} \\
H_a&: \mathrm{The \ difference \ of \ population \ means \ for \ our \ variables \ is \ not \ equal \ to \ the \ previously \ believed \ value.} \\
&\mathrm{or} \\
H_a&: \mathrm{The \ difference \ of \ population \ means \ for \ our \ variables \ is \ less \ than \ the \ previously \ believed \ value.} \\
&\mathrm{or} \\
H_a&: \mathrm{The \ difference \ of \ population \ means \ for \ our \ variables \ is \ greater \ than \ the \ previously \ believed \ value.}
\end{align*}

\item The assumptions for the hypothesis test are the same as the assumptions for the two independent means confidence interval.

\end{enumerate}

\section{Hypothesis Tests for One Population Proportion}
\section{Hypothesis Tests Two Independent Population Proportions}
\section{Hypothesis Test for Goodness of fit}
\section{Hypothesis Test for Homogeneity}
\section{Hypothesis Test for Independence}
\section{Hypothesis Test for Correlation}