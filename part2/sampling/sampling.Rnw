\chapter{The Sampling Distribution}

\section{Basic Ideas}

We want to learn about populations from data collected in samples.
Until now, we have only briefly discussed the ideas of population
and sample distributions.

\begin{defn}
Population Distribution: The distribution for all values that are
possible for an \textbf{individual outcome} from the population of
interest.
\end{defn}

\begin{defn}
Sample Distribution: The distribution of values for an \textbf{individual outcome} which actually occurred when we collected our sample data.
\end{defn}

An important thing to keep in mind is that the purpose of collecting
a sample is to collect data that is representative of the population.
So, if we were to graph our sample data, it should look similar to
the graph of the population distribution. However, it is obvious that
if instead of taking one sample, we take many samples, each of those
samples outcomes will be different. This is known as sampling variability.

We are left with two problems: calculating probabilities for unknown
distributions and the variability of the probabilities for individual
outcomes are relatively high based on sample data.

We have seen how to calculate probabilities for normal distributions
in the previous chapter. However, we learned that there are an infinite
number of possible population distributions and do not have the tools
to determine these populations or calculate probabilities for them.
So, it would be helpful if there was some result which would allow
us to use the normal distribution and at the same time reduce the
problem of sampling variability. Naturally this result should also
be useful to us. That is, its results will at least be able to infer
back on the average outcome we would observe in the population.

\section{The Central Limit Theorem}

The central limit theorem is one of the most important results in
statistics. Its results allow us to easily find probabilities that
in turn will allow us make inferences on the original population.
These probabilities are not of individual outcomes, they are of the
mean outcome. Hence, we must define this new distribution of the sample
mean.

\begin{defn}
Sampling Distribution: The distribution of all possible values of
the \textbf{sample mean}.
\end{defn}

\begin{defn}
Central Limit Theorem (Informal): The distribution of $\bar{x}$ (the
sampling distribution) is approximately normal. This approximation
is poor for a small sample size. This approximation is very good for
large sample sizes.
\end{defn}

\begin{theorem}
Central Limit Theorem

For any{*} population distribution, if our sample from that distribution
has a large sample size $\left(n\geq30\right)$, then

\begin{displaymath}
\bar{x}\sim N\left(\mu,\left(\frac{\sigma}{\sqrt{n}}\right)^{2}\right)
\end{displaymath}

where $\mu$ and $\sigma$ are the parameters of the original population
distribution.
\end{theorem}

This result is important because there is no assumption on the population distribution. For any population distribution, if we take a large sample from it, then the sampling distribution is known.

\subsubsection{Sampling Distribution Summary}

Suppose that a variable $X$ of a population has mean $\mu$ and standard
deviation $\sigma$. Then, for samples of size $n$,

\begin{enumerate}
\item The mean of $\bar{x}$ (mean of the possible sample means) is equal
to the population mean
\item The standard deviation of $\bar{x}$ equals the population standard
deviation divided by the square root of the sample size

\begin{align*}
\sigma_{\bar{x}} &= \frac{\sigma}{\sqrt{n}}
\end{align*}

\item If $X\sim N(\mu,\,\sigma^{2})$ (the population distribution is normal), then so is $\bar{x}$ no matter the sample size.
\item If $n\geq30$, then $\bar{x}\sim N\left(\mu,\,\left(\frac{\sigma}{\sqrt{n}}\right)^{2}\right)$ regardless of the population distribution of $X$.
\end{enumerate}

\subsubsection{Big Idea}

How does this fit in to what we want in statistics? We want to make
logical conclusions about the population distribution using the sample
distribution. Given that we have some prior assumption about the population distribution, we will use the sample results to compare to this prior assumption. In other words, we will find the probability of observing our sample mean (or a sample mean more extreme) given that our prior assumption of the population distribution is true. We will determine the sampling distribution that is based on the population distribution parameters and see how our collected sample mean compares.