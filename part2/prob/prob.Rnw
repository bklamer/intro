\chapter{Rules of Probability}

Before learning about inferential statistics, we need to explore some of
the theory that builds its foundation. Studying probability will allow us to evaluate and
control the likelihood that our statistical inference is correct.

\section{Essential Definitions}

\begin{defn}
Probability: The (hypothetical) chance of an event happening. For the purposes of this class, we will also use the proportion of times an outcome would occur in a very long series of repetitions.
\end{defn}

\begin{defn}
Sample Space: The sample space (also known as the probability space) is the collection
of all possible outcomes.
\end{defn}

\begin{defn}
Event: One or more outcomes that are a subset of the sample space.
We say that ``event $A$ has occurred'' if the outcome was one of
the sample points in $A$.
\end{defn}

\begin{defn}
Mutually Exclusive Events: Events are mutually exclusive (disjoint)
if they cannot have any outcomes in common.
\end{defn}

\begin{itemize}
\item When you roll a die, you cannot roll both a 1 and a 2.
We would say that events 1 and 2 are mutually exclusive.
\end{itemize}

\begin{defn}
Union of Events: The union of two or more events consists of all sample
points that are in the events.
\end{defn}

\begin{defn}
Intersection of Events: The intersection of two or more events consists
of all sample points that are shared between all events.
\end{defn}

\begin{defn}
Complement: The complement of event $A$ consists of all sample points
in the sample space that are not in $A$.
\end{defn}

\begin{defn}
Independent Events: Events $A$ and $B$ are called independent if
knowing that one will occur does not change the probability that the
other occurs.
\end{defn}

\begin{defn}
Dependent Events: Events $A$ and $B$ are called dependent if knowing
that one will occur affects the probability of the other event.
\end{defn}

\section{Probability}

We will approach the concept of probability with the frequentist perspective. This means we will define probability as the proportion of times the outcome would occur in a very long series of repetitions. You may think of it as the following

\begin{align*}
\text{Probability of event} &= \frac{\text{Number of ways an event can occur}}{\text{Total number of possible outcomes}}.
\end{align*}

\subsection{Basic Probability}

\subsubsection{Probability Notation}

If $A$ is an event, then $P(A)$ represents the probability that
event $A$ occurs. $P(A)$ is read as ``the probability of $A$''.
We can extend this to previously mentioned ideas:

\begin{align*}
P(A\cap B) &= \text{Probability of \textit{A} and \textit{B} both occuring} \\
P(A\cup B) &= \text{Probability of \textit{A} or \textit{B} occuring} \\
P\left( A^{c} \right) &= \text{Probability of the complement of \textit{A} occuring}
\end{align*}

\subsubsection{Basic Probability Rules}

See the full list in section \ref{probability-rules}.

\begin{enumerate}
\item The probability of any event $A$, $P(A)$, satisfies $0\leq P(A)\leq1$.
\item If $S$ is the sample space, then $P(S)=1$
\item For any two events $A$ and $B$,
\begin{align*}
P(A\cup B) &= P(A)+P(B)-P(A\cap B)
\end{align*}
\item If $A$ and $B$ are disjoint, then $P(A\cap B)=0$. So, for disjoint
$A$ and $B$,
\begin{align*}
P(A\cup B) &= P(A)+P(B)
\end{align*}
\item For any event $A$,
\begin{align*}
P(A\, does\, not\, occur) &= 1-P(A) \\
&= P(A^{c})
\end{align*}
\end{enumerate}

Venn diagrams can be useful for getting across
these ideas. See figure \ref{basic-venn-diagrams} for visual examples of samples spaces, intersections, unions, and mutually exclusive events

\begin{figure}[H]
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[scale=0.4]{part2/prob/graphs/venn_diagram_1}
  \caption{Basic Venn Diagram}
  \label{basic-venn-diagram}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[scale=0.4]{part2/prob/graphs/intersection}
  \caption{Intersection of events $A$ and $B$}
  \label{intersection}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[scale=0.4]{part2/prob/graphs/union}
  \caption{Union of events $A$ and $B$}
  \label{union}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[scale=0.4]{part2/prob/graphs/mutually_exclusive}
  \caption{Mutually Exclusive Events}
  \label{mutally-exclusive}
\end{subfigure}%
\caption{Basic Venn Diagrams}
\label{basic-venn-diagrams}
\end{figure}

\subsection{Conditional Probability and Independence}

The Probability that an event $A$ occurs given that an event $B$
has occurred is called a conditional probability. We denote this as
$P(\left. A \right| B)$ which is read as ``the probability of $A$
given $B$''. This conditional probability is often computed using
unconditional probabilities with the formula

\begin{align*}
P(\left. A \right| B) &= \frac{P(A \cap B)}{P(B)}.
\end{align*}

Using a bit of algebra leads us to another finding: $P(A \cap B) = P(B)P(\left. A \right| B)$. Changing the order of events in the conditional probability leads to a predictable outcome

\begin{align*}
P(\left. B \right| A) &= \frac{P(A \cap B)}{P(A)} \ , \ \mathrm{and} \\
P(A \cap B) &= P(A)P(\left. B \right| A) \\
&= P(B)P(\left. A \right| B).
\end{align*}

This result applies to all cases of events $A$ and $B$. Specifically,
if whether or not they are independent or dependent.

Two events are called independent if knowing that one occurs does
not change the probability that the other occurs. If $A$ and $B$
are independent,

\begin{align*}
P(A \cap B) &= P(A)P(B).
\end{align*}

Again, applying a bit of algebra, we have another useful result,

\begin{align*}
P(\left. B \right| A) &= P(B) \ , \ \mathrm{and} \\
P(\left. A \right| B) &= P(A).
\end{align*}

\subsubsection{Marginal and Joint Probabilities in Contingency Tables}

Contingency tables are often used to present frequency counts for
categorical variables. This is done to analyze the relationship between
two or more categorical variables.

The marginal probability is the probability of an event with no conditioning. So, the marginal probability of event $A$ is $P(A)$. The joint probability is the probability that both events occur. If we have event $A$ and $B$ then the joint probability is $P(A\cap B)$. Consider the following table,

\begin{table}[H]
\caption{Marginal and Joint Probabilities}
\begin{tabular}{|c|c|c|c|c|c|}
\cline{3-6} 
\multicolumn{1}{c}{} &  & \multicolumn{4}{c|}{Variable 1}\tabularnewline
\cline{3-6} 
\multicolumn{1}{c}{} & Event & A1 & A2 & A3 & Total\tabularnewline
\hline 
\multirow{5}{*}{Variable 2} & B1 & $A1\cap B1$ & $A2\cap B1$ & $A3\cap B1$ & $B1$\tabularnewline
\cline{2-6} 
 & B2 & $A1\cap B2$ & $A2\cap B2$ & $A3\cap B2$ & $B2$\tabularnewline
\cline{2-6} 
 & B3 & $A1\cap B3$ & $A2\cap B3$ & $A3\cap B3$ & $B3$\tabularnewline
\cline{2-6} 
 & B4 & $A1\cap B4$ & $A2\cap B4$ & $A3\cap B4$ & $B4$\tabularnewline
\cline{2-6} 
 & Total & $A1$ & $A2$ & $A3$ & 100\%\tabularnewline
\hline 
\end{tabular} 
\end{table}

Now we will consider the following example that looks at the variables
$major$ and $starting\, salary$. The values within each cell are
the observed frequencies for the sample.

\begin{table}[H]
\caption{Marginal and Joint Probabilities Example}
\begin{tabular}{|c|c|c|c|c|c|}
\cline{3-6} 
\multicolumn{1}{c}{} &  & \multicolumn{4}{c|}{Starting Salary}\tabularnewline
\cline{3-6} 
\multicolumn{1}{c}{} & Event & Under \$20,000 & \$20,000-\$30,000 & \$30,000+ & Total\tabularnewline
\hline 
\multirow{5}{*}{Major} & A\&S & 10 & 55 & 15 & 80\tabularnewline
\cline{2-6} 
 & Business & 25 & 30 & 5 & 60\tabularnewline
\cline{2-6} 
 & Engineering & 5 & 20 & 50 & 75\tabularnewline
\cline{2-6} 
 & Psychology & 5 & 15 & 5 & 25\tabularnewline
\cline{2-6} 
 & Total & 45 & 120 & 75 & 240\tabularnewline
\hline 
\end{tabular}
\end{table}

Suppose we are interested the following probabilities, $P(Under \ 20,000)$, $P(Psychology)$, and $P(\left. Under \ 20,000 \right| Psychology)$. We can use the frequencies directly and use the calculation rule of probability, or we can convert the table to individual probabilities instead of frequencies. We find that

\begin{align*}
P(Under \ 20,000) &= \frac{45}{240} \\
&= 0.1875 \\
\\
P(Psychology) &= \frac{25}{240} \\
&= 0.104 \\
\\
P(\left. Under \ 20,000 \right| Psychology) &= \frac{P(Under \ 20,000 \cap Psychology)}{P(Psychology)} \\
&= \frac{5/240}{25/240} \\
&= \frac{5}{25} \\
&= 0.2
\end{align*}

Notice how the probabilities changed between $P(Under \ 20,000)$
and $P(\left.Under \ 20,000 \right|Psychology)$. Since the
probability did not stay the same, we conclude that getting a salary
under \$20,000 and being a psychology major are dependent. In essence,
knowing that someone is a psychology major changes the probability
of earning under \$20,000.

\section{Probability Rules}
\label{probability-rules}

\begin{enumerate}
\item The probability of any event $A$, $P(A)$, satisfies $0\leq P(A)\leq1$.
\item If $S$ is the sample space, then $P(S)=1$
\item For any two events $A$ and $B$
\begin{align*}
P(A\cup B) &= P(A)+P(B)-P(A\cap B)
\end{align*}
\item If $A$ and $B$ are disjoint, then $P(A\cap B)=0$. So, for disjoint
$A$ and $B$,
\begin{align*}
P(A\cup B) &= P(A)+P(B)
\end{align*}
\item For any event $A$,
\begin{align*}
P(A\, does\, not\, occur) &= 1-P(A) \\
&= P(A^{c})
\end{align*}
\item $A$ and $B$ can be dependent \textbf{or} independent,
\begin{align*}
P(\left. A \right| B) &= \frac{P(A \cap B)}{P(B)} \text{, and} \\
P(\left. B \right| A) &= \frac{P(A \cap B)}{P(A)}
\end{align*}
\item $A$ and $B$ can be dependent \textbf{or} independent,
\begin{align*}
P(A \cap B) &= P(A)P(\left. B \right| A) \\
&= P(B)P(\left. A \right| B)
\end{align*}
\item If and only if $A$ and $B$ are independent,
\begin{align*}
P(\left. A \right| B) &= P(A) \text{, and} \\
P(\left. B \right| A) &= P(B)
\end{align*}
\item If and only if $A$ and $B$ are independent,
\begin{align*}
P(A \cap B) &= P(A)P(B)
\end{align*}
\end{enumerate}

\section{Probability Distributions}

Probability distributions are a way of expressing all possible events
and the probability for each event.

Rules for probability distributions:

\begin{enumerate}
\item The outcomes must be disjoint.
\item Each probability must be between 0 and 1.
\item The probabilities must add up to 1.
\end{enumerate}

Consider Table 3, our sample space of flipping a coin two times is
$S=\{HH,\, HT,\, TH,\, TT\}$.

\begin{table}[H]
\caption{Probability Distribution for Flipping a Coin Twice}
\begin{tabular}{|c|c|}
\hline 
Event & Probability\tabularnewline
\hline 
\hline 
HH & 0.25\tabularnewline
\hline 
HT & 0.25\tabularnewline
\hline 
TH & 0.25\tabularnewline
\hline 
TT & 0.25\tabularnewline
\hline 
\end{tabular}
\end{table}

If we were interested in the number of heads in two coin flips, the
sample space would be $S=\{0,\,1,\,2\}$. Table 4 lists the probabilities
associated with each of these events.

\begin{table}[H]
\caption{Probability Distribution for Number of Heads in Flipping a Coin Twice}

\begin{tabular}{|c|c|}
\hline 
Event & Probability\tabularnewline
\hline 
\hline 
0 & 0.25\tabularnewline
\hline 
1 & 0.5\tabularnewline
\hline 
2 & 0.25\tabularnewline
\hline 
\end{tabular}
\end{table}

We can also consider the sum of rolling two dice. The sample space
is $S=\{2,\,3,\,4,\,5,\,6,\,7,\,8,\,9,\,10,\,11,\,12\}$. Table 5
lists the probabilities associated with each of these events.

\begin{table}[H]
\caption{Probability Distribution for Sum of Rolling Two Dice}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline 
Event & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12\tabularnewline
\hline 
Probability & $\frac{1}{36}$ & $\frac{2}{36}$ & $\frac{3}{36}$ & $\frac{4}{36}$ & 
$\frac{5}{36}$ & $\frac{6}{36}$ & $\frac{5}{36}$ & $\frac{4}{36}$ & $\frac{3}{36}$ & 
$\frac{2}{36}$ & $\frac{1}{36}$\tabularnewline
\hline 
\end{tabular}
\end{table}

\section{Practice Questions} \hspace{1pt} \\

\framebox{\begin{minipage}{0.97\linewidth}
\begin{problem}
The probability of forgetting to bring a calculator to
a statistics exam is $\frac{1}{11}$. Assume that this is an independent
process.
\begin{enumerate}[a)]
\item What is the probability of one randomly chosen student \textbf{not}
forgetting a calculator?
\item What is the probability that all 3 randomly chosen students
have not forgotten to bring a calculator?
\item What is the probability that all 3 randomly chosen students
have forgotten to bring a calculator?
\item What is the probability that at least one of the three randomly
chosen students have forgotten to bring a calculator?
\end{enumerate}
\end{problem}
\end{minipage}
}

\begin{solution} \hspace{1pt} \\
\begin{enumerate}[a)]
\item 
\begin{align*}
P(not \ forgetting) &= 1-P \left( (not \ forgetting)^c \right) \\
&= 1 - P(forgetting) \\
&= 1 - \frac{1}{11} \\
&= \frac{10}{11} \\
&= 0.9091
\end{align*}

\item Because we have an independent process, we can use the formula

\begin{align*}
P(all \ 3 \ not \ forgetting) &= P(first_{not \ forget} \cap second_{not \ forget} \cap third_{not \ forget}) \\
&= P(not \ forgetting)P(not \ forgetting)P(not \ forgetting) \\
&= \left( \frac{10}{11} \right)\left( \frac{10}{11} \right)\left( \frac{10}{11} \right) \\
&= 0.7513
\end{align*}

\item Because we have an independent process, we can use the formula

\begin{align*}
P(all \ 3 \ forgetting) &= P(first_{forget} \cap second_{forget} \cap third_{forget}) \\
&= P(forgetting)P(forgetting)P(forgetting) \\
&= \left( \frac{1}{11} \right)\left( \frac{1}{11} \right)\left( \frac{1}{11} \right) \\
&= 0.0007513
\end{align*}

\item Because we have an independent process, we can use the formula

\begin{align*}
P(at \ least \ one \ of \ 3 \ forgetting) &= 1-P \left( (at \ least \ one \ of \ 3 \ forgetting)^c \right) \\
&= 1-P \left( all \ not \ forgetting \right) \\
&= 1-P(not \ forgetting)P(not \ forgetting)P(not \ forgetting) \\
&= 1-0.7513 \\
&= 0.2487
\end{align*}

\end{enumerate}
\end{solution}

\framebox{\begin{minipage}{0.97\linewidth}
\begin{problem}

The following table contains data on cause of death and
occupation for a sample of 500 people. Frequencies for the corresponding
events are shown in the table.

\begin{tabular}{c|c|c|c|c|c|c|}
\cline{3-6} 
\multicolumn{1}{c}{} &  & \multicolumn{4}{c|}{Occupation} & \multicolumn{1}{c}{}\tabularnewline
\cline{3-7} 
\multicolumn{1}{c}{} &  & Police & Cashiers & Taxi Drivers & Guards & Total\tabularnewline
\hline 
\multicolumn{1}{|c|}{\multirow{2}{*}{Cause of Death}} & Homicide & 82 & 104 & 70 & 64 & 320\tabularnewline
\cline{2-7} 
 & Not Homicide & 92 & 23 & 29 & 36 & 180\tabularnewline
\hline 
 & Total & 174 & 127 & 99 & 100 & 500\tabularnewline
\cline{2-7} 
\end{tabular}

\begin{enumerate}[a)]
\item What is the probability that cause of death was from homicide?
\item What is the probability that the occupation was a guard?
\item What is the probability that cause of death was from homicide
given that the persons occupation was being a guard?
\item Does it appear that ``cause of death being homicide'' and ``occupation being a guard'' are independent? Why or why not? 
\end{enumerate}

\end{problem}
\end{minipage}
}

\begin{solution} \hspace{1pt} \\
\begin{enumerate}[a)]
\item $P(Homicide)=\frac{320}{500}=0.64$
\item $P(Guard)=\frac{100}{500}=0.2$
\item $P(Homicide|Guard)=\frac{64}{100}=0.64$
\item Since $P(Homicide|Guard)=0.64=P(Homicide)$, then by the laws of conditional probability, these two events are independent.
\end{enumerate}
\end{solution}